<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<div class="col-lg-8">
<!-- content -->
<div class="trm-content" id="trm-content">
<div data-scroll data-scroll-repeat data-scroll-offset="500" id="about-triger">
</div>

<div class="col-lg-12">
<div class="trm-counter-up trm-scroll-animation trm-active-el" data-scroll="" data-scroll-offset="40">
<div class="trm-counter-number"><?php $this->archiveTitle([
            'category' => _t('分类<span class="trm-counter-symbol" style="font-size: 24px;"> %s </span>下的文章'),
            'search'   => _t('包含关键字<span class="trm-counter-symbol" style="font-size: 24px;"> %s </span>的文章'),
            'tag'      => _t('标签<span class="trm-counter-symbol" style="font-size: 24px;"> %s </span>下的文章'),
            'author'   => _t('<span class="trm-counter-symbol" style="font-size: 24px;"> %s </span>发布的文章')
        ], '', ''); ?></div>
</div>
</div>


<div class="row" id="main" role="main">
    <?php if ($this->have()): ?>
        <?php while ($this->next()): ?>
            <div class="col-lg-6">
<div class="trm-blog-card trm-scroll-animation" data-scroll data-scroll-offset="40">
<a class="trm-cover-frame trm-anima-link" href="<?php $this->permalink() ?>" title="<?php $this->title() ?>">
<?php $thumbnailUrl = get_ArticleThumbnail($this); if ($thumbnailUrl): ?>  
    <img src="<?php echo htmlspecialchars($thumbnailUrl); ?>" alt="文章缩略图">  
<?php endif; ?>
</a>
<div class="trm-card-descr">
<h5 class="trm-mb-20"><a class="trm-anima-link" href="<?php $this->permalink() ?>" title="<?php $this->title() ?>"><?php $this->title() ?></a></h5>
<div class="trm-divider trm-mb-20 trm-mt-20"></div>
<ul class="trm-card-data trm-label">
<li datetime="<?php $this->date('c'); ?>" itemprop="datePublished"><?php $this->date(); ?></li>
<li><?php $this->category(','); ?></li>
</ul>
</div>
</div>
</div>
        <?php endwhile; ?>
    <?php else: ?>
        <article class="post">
            <h2 class="post-title"><?php _e('没有找到内容'); ?></h2>
        </article>
    <?php endif; ?>

    <?php $this->pageNav('&laquo; 前一页', '后一页 &raquo;'); ?>
</div>

</div>
</div>
<?php $this->need('sidebar.php'); ?>
<?php $this->need('footer.php'); ?>
