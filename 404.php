<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<div class="col-lg-8">
<!-- content -->
<div class="trm-content" id="trm-content">
<div data-scroll data-scroll-repeat data-scroll-offset="500" id="about-triger">
</div>

<div class="col-lg-12">
<div class="trm-counter-up trm-scroll-animation trm-active-el" data-scroll="" data-scroll-offset="40">
<div class="trm-counter-number">
<?php echo yiyan() ?>
</div>
</div>
</div>


<div class="col-mb-12 col-tb-8 col-tb-offset-2">

    <div class="trm-card trm-publication">
        <div class="text-center">
        <img src="<?php $this->options->themeUrl('/img/null.svg'); ?>" style="margin-bottom: 20px;">
        <p style="margin-bottom: 20px;"><?php _e('你想查看的页面已被转移或删除了, 要不要搜索看看: '); ?></p>
        <form method="post">
            <p><input type="text" name="s" class="text" autofocus/></p>
                <button class="trm-btn" type="submit" class="submit"><?php _e('搜索'); ?></button>
        </form>
        </div>
    </div>

</div><!-- end #content-->
<?php $this->need('footer.php'); ?>
