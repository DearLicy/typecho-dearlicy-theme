<?php
/**
 * 官网<a href="https://www.vxras.com">初一小盏</a><br>交流群：770351424<br>1.1更新内容<br>优化图片灯箱<br>修复PJAX切页导致的日夜间切换异常
 *
 * @package DearLicy
 * @author 李初一
 * @version 1.1
 * @link https://www.vxras.com
 */

if (!defined('__TYPECHO_ROOT_DIR__')) exit;
$this->need('header.php');
?>

<div class="col-lg-8">
<!-- content -->
<div class="trm-content" id="trm-content">
<div data-scroll data-scroll-repeat data-scroll-offset="500" id="about-triger">
</div>
<!-- stats -->
<div class="row">
<div class="col-lg-4">
<!-- counter -->
<?php Typecho_Widget::widget('Widget_Stat')->to($stat); ?>
<div class="trm-counter-up trm-scroll-animation" data-scroll data-scroll-offset="40">
<div class="trm-counter-number">
<span class="trm-counter"><?php $stat->publishedPostsNum() ?></span>
<span class="trm-counter-symbol">✔</span>
</div>
<div class="trm-divider trm-mb-20 trm-mt-20"></div>
<div class="trm-label">文章总数</div>
</div>
<!-- counter end -->
</div>
<div class="col-lg-4">
<!-- counter -->
<div class="trm-counter-up trm-scroll-animation" data-scroll data-scroll-offset="40">
<div class="trm-counter-number"><span class="trm-counter"><?php $stat->publishedCommentsNum() ?></span>
<span class="trm-counter-symbol">✔</span></div>
<div class="trm-divider trm-mb-20 trm-mt-20"></div>
<div class="trm-label">评论总数</div>
</div>
<!-- counter end -->
</div>
<div class="col-lg-4">
<!-- counter -->
<div class="trm-counter-up trm-scroll-animation" data-scroll data-scroll-offset="40">
<div class="trm-counter-number"><span class="trm-counter"><?php echo online_users() ?></span>
<span class="trm-counter-symbol">✔</span></div>
<div class="trm-divider trm-mb-20 trm-mt-20"></div>
<div class="trm-label">在线人数</div>
</div>
<!-- counter end -->
</div>
</div>
<!-- stats end -->
<!-- text -->
<div class="row">
<div class="col-lg-12">
<!-- title -->
<h5 class="trm-mb-40 trm-title-with-divider">介绍 <span data-number="<?php echo getNextNumber() ?>"></span></h5>
<!-- quote -->
<blockquote class="trm-scroll-animation" data-scroll data-scroll-offset="40"><?php $this->options->desc() ?></blockquote>
<!-- quote end -->
</div>
</div>
<!-- text end -->
<!-- video resume -->
<div class="row">
<div class="col-lg-12">
<!-- title -->
<h5 class="trm-mb-40 trm-mt-40 trm-title-with-divider">视频<span data-number="<?php echo getNextNumber() ?>"></span></h5>
</div>
<div class="col-lg-12">
<!-- video -->
<div class="trm-video trm-scroll-animation" data-scroll data-scroll-offset="40">
<div class="trm-video-content trm-overlay"><img src="https://typecho.vxras.com/usr/themes/DearLicy/img/articles/1.avif" alt="video-cover">
<div class="trm-button-puls"></div>
<a data-fancybox="" href="https://youtu.be/8vZJYFC6GOo" class="trm-play-button"><i class="fas fa-play"></i></a>
</div>
</div>
<!-- video end -->
</div>
</div>
<!-- video resume end -->


<!-- blog -->
<div class="row">
<div class="col-lg-12">
<!-- title -->
<h5 class="trm-mb-40 trm-mt-40 trm-title-with-divider">最新发布 <span data-number="<?php echo getNextNumber() ?>"></span>
</h5>
</div>

<!-- blog card -->
<?php while ($this->next()): ?>
<div class="col-lg-6">
<div class="trm-blog-card trm-scroll-animation" data-scroll data-scroll-offset="40">
<a class="trm-cover-frame trm-anima-link" href="<?php $this->permalink() ?>" title="<?php $this->title() ?>">
<?php $thumbnailUrl = get_ArticleThumbnail($this); if ($thumbnailUrl): ?>  
    <img src="<?php echo htmlspecialchars($thumbnailUrl); ?>" alt="文章缩略图">  
<?php endif; ?>
</a>
<div class="trm-card-descr">
<h5 class="trm-mb-20"><a class="trm-anima-link" href="<?php $this->permalink() ?>" title="<?php $this->title() ?>"><?php $this->title() ?></a></h5>
<div class="trm-divider trm-mb-20 trm-mt-20"></div>
<ul class="trm-card-data trm-label">
<li datetime="<?php $this->date('c'); ?>" itemprop="datePublished"><?php $this->date(); ?></li>
<li><?php $this->category(','); ?></li>
</ul>
</div>
</div>
</div>
<?php endwhile; ?>

    <?php $this->pageNav('&laquo; 前一页', '后一页 &raquo;'); ?>
</div><!-- end #main-->

<?php /*$this->need('sidebar.php');*/ ?>
<?php $this->need('footer.php'); ?>
