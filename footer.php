<div class="trm-divider trm-mb-40"></div>
<!-- footer -->
<footer class="trm-scroll-animation" data-scroll data-scroll-offset="50">
<div class="trm-label">© 2024 All Rights Reserved.<br>Designed by:<a href="<?php $this->options->siteurl() ?>" target="_blank"><?php $this->options->title() ?></a></div>
<br>
<div class="trm-label">
<img src="https://www.xfabe.com/zb_users/theme/quietlee/style/images/icp.png" alt="<?php $this->options->beian() ?>">
<a href="http://beian.miit.gov.cn/"><?php $this->options->beian() ?></a><br>
<a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=" style="margin-left: 23px;"><?php $this->options->gongan() ?></a>
</div>
</footer>
<!-- footer end -->
</div>
<!-- content end -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- app wrapper end -->
<!-- jquery js -->
<script src="<?php $this->options->themeUrl('/js/plugins/jquery.min.js'); ?>"></script><!-- swup js -->
<script src="<?php $this->options->themeUrl('/js/plugins/swup.min.js'); ?>"></script><!-- locomotive scroll js -->
<script src="<?php $this->options->themeUrl('/js/plugins/locomotive-scroll.js'); ?>"></script><!-- typing js -->
<script src="<?php $this->options->themeUrl('/js/plugins/typing.js'); ?>"></script><!-- fancybox js -->
<script src="<?php $this->options->themeUrl('/js/plugins/fancybox.min.js'); ?>"></script><!-- swiper js -->
<script src="<?php $this->options->themeUrl('/js/plugins/swiper.min.js'); ?>"></script><!-- 李初一 js -->
<script src="<?php $this->options->themeUrl('/js/main.js'); ?>"></script>
<script src="<?php $this->options->themeUrl('/js/copy.js'); ?>"></script>

<?php $this->footer(); ?>
</body>
</html>