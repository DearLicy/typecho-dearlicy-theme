<?php 
/**
* 友情链接
*
* @package custom
*/
if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>
<div class="col-lg-8">
<!-- content -->
<div class="trm-content" id="trm-content">
<div data-scroll="" data-scroll-repeat="" data-scroll-offset="500" id="about-triger" class="trm-active-el"></div>
<div class="row">
<div class="col-lg-4">
<div class="trm-card trm-label trm-label-light text-center"><i class="far fa-calendar-alt trm-icon"></i><br><?php $this->date(); ?></div>
</div>
<div class="col-lg-4">
<div class="trm-card trm-label trm-label-light text-center"><i class="far fa-clock trm-icon"></i><br><?php echo date('H:i:s', $this->created); ?></div>
</div>
<div class="col-lg-4">
<div class="trm-card trm-label trm-label-light text-center"><i class="far fa-user trm-icon"></i><br><?php $this->author(); ?></div>
</div>
</div>

<div class="col-lg-12">
<h5 class="trm-mb-40 trm-title-with-divider">正文<span data-number="<?php echo getNextNumber() ?>"></span></h5>
</div>

<div class="trm-card trm-publication">
<?php    
  
// 为<pre>标签添加class="notranslate"    
$prePattern = '/<pre(.*?)>/i';    
$preReplacement = '<pre class="notranslate" $1>'; // 注意在class后面添加了一个空格  
$content = preg_replace($prePattern, $preReplacement, $this->content);    
    
// 为<blockquote>标签添加class    
$blockquotePattern = '/<blockquote(.*?)>/i';    
$blockquoteReplacement = '<blockquote class="trm-color-quote trm-mb-40" $1>'; // 同样添加了空格  
$content = preg_replace($blockquotePattern, $blockquoteReplacement, $content); // 注意这里使用$content而不是$this->content（如果$this->content是有效的）  
  
// 为<ul>标签添加class    
$ulPattern = '/<ul(.*?)>/i';    
$ulReplacement = '<ul class="trm-list trm-mb-40" $1>'; // 同样添加了空格  
$content = preg_replace($ulPattern, $ulReplacement, $content); // 同样使用$content  
  
echo $content;    
?>
</div>

<div class="col-lg-12">
<h5 class="trm-mb-40 trm-title-with-divider">友链列表<span data-number="<?php echo getNextNumber() ?>"></span></h5>
</div>

<div class="trm-card">


<?php if(!empty(trim($this->options->friendLink))): ?>  
    <?php   
        $friendLinks = explode("\n", $this->options->friendLink); // 按行分割  
        foreach ($friendLinks as $link):  
            $parts = explode('|', $link); // 按 '|' 字符分割  
            if (count($parts) == 4): // 确保分割后有两个部分（标题和URL）  
                $title = htmlspecialchars($parts[0]);  
                $url = htmlspecialchars($parts[1]);  
                $desc = htmlspecialchars($parts[2]);  
                $avatar = htmlspecialchars($parts[3]);  
                ?>  
<div class="author-minicard links-card radius8">
                <ul class="list-inline">
                    <li><a class="avatar-img link-img" href="<?php echo $url; ?>"><img class="avatar lazyloaded" src="<?php echo $avatar; ?>" data-src="<?php echo $avatar; ?>"></a>
                    </li>
                    <li>
                        <dl>
                            <dt><a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></dt>
                            <dd class="avatar-dest em09 muted-3-color text-ellipsis"><?php echo $desc; ?></dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <?php   
            endif;  
        endforeach;  
    ?>  
<?php endif; ?>


</div>

<div class="row">
<div class="col-lg-12">
<h5 class="trm-mb-40 trm-title-with-divider">评论<span data-number="<?php echo getNextNumber() ?>"></span></h5>
</div>
</div>
<?php $this->need('comments.php'); ?>


<?php $this->need('sidebar.php'); ?>
<?php $this->need('footer.php'); ?>
