<?php
if (!defined('__TYPECHO_ROOT_DIR__')) exit;

function themeConfig($form)
{
    $logoUrl = new \Typecho\Widget\Helper\Form\Element\Text(
        'logoUrl',
        null,
        null,
        _t('站点 LOGO 地址'),
        _t('在这里填入一个图片 URL 地址, 以在网站标题前加上一个 LOGO')
    );

    $form->addInput($logoUrl);

    $favicon = new \Typecho\Widget\Helper\Form\Element\Text(
        'favicon',
        null,
        null,
        _t('站点 favicon 图标'),
        _t('在这里填入一个图片 URL 地址, 设置网站的favicon图标')
    );

    $form->addInput($favicon);

    $beian = new \Typecho\Widget\Helper\Form\Element\Text(
        'beian',
        null,
        null,
        _t('备案号号'),
        _t('在这里填入你的备案号')
    );

    $form->addInput($beian);

    $gongan = new \Typecho\Widget\Helper\Form\Element\Text(
        'gongan',
        null,
        null,
        _t('公安备案号号'),
        _t('在这里填入你的公安备案号如果有的话')
    );

    $form->addInput($gongan);

    $bg1 = new \Typecho\Widget\Helper\Form\Element\Text(
        'bg1',
        null,
        null,
        _t('首页背景'),
        _t('在这里填入背景链接')
    );

    $form->addInput($bg1);

    $bg2 = new \Typecho\Widget\Helper\Form\Element\Text(
        'bg2',
        null,
        null,
        _t('非首页背景'),
        _t('在这里填入背景链接')
    );

    $form->addInput($bg2);

    $light = new \Typecho\Widget\Helper\Form\Element\Text(
        'light',
        null,
        null,
        _t('日间主题色'),
        _t('在这里填入颜色代码')
    );

    $form->addInput($light);

    $dark = new \Typecho\Widget\Helper\Form\Element\Text(
        'dark',
        null,
        null,
        _t('夜间主题色'),
        _t('在这里填入颜色代码')
    );

    $form->addInput($dark);

    $qq = new \Typecho\Widget\Helper\Form\Element\Text(
        'QQ',
        null,
        null,
        _t('QQ'),
        _t('在这里填入QQ，用于获取头像')
    );

    $form->addInput($qq);

    $email = new \Typecho\Widget\Helper\Form\Element\Text(
        'email',
        null,
        null,
        _t('email'),
        _t('在这里填入email，用于前端联系按钮')
    );

    $form->addInput($email);

    $github = new \Typecho\Widget\Helper\Form\Element\Text(
        'github',
        null,
        null,
        _t('github'),
        _t('在这里填入github，用于前端联系按钮')
    );

    $form->addInput($github);

    $telegraph = new \Typecho\Widget\Helper\Form\Element\Text(
        'telegraph',
        null,
        null,
        _t('telegraph'),
        _t('在这里填入telegraph，用于前端联系按钮')
    );

    $form->addInput($telegraph);

    $twitter = new \Typecho\Widget\Helper\Form\Element\Text(
        'twitter',
        null,
        null,
        _t('twitter'),
        _t('在这里填入twitter，用于前端联系按钮')
    );

    $form->addInput($twitter);

    $desc = new \Typecho\Widget\Helper\Form\Element\Text(
        'desc',
        null,
        null,
        _t('首页介绍'),
        _t('在这里输入文字介绍你得站点')
    );

    $form->addInput($desc);

    $articleImgSpeed  = new Typecho_Widget_Helper_Form_Element_Text('articleImgSpeed', NULL, NULL, _t('文章缩略图加速'), _t('填入加速地址前段，不要填图片地址的序号和.jpg（默认jpg图片格式）缩略图数量根据主题预设的张数固定了。例: https://cdn.xxxx.com/images/articles/'));
    $form->addInput($articleImgSpeed);

    // $sidebarBlock = new \Typecho\Widget\Helper\Form\Element\Checkbox(
    //     'sidebarBlock',
    //     [
    //         'ShowRecentPosts'    => _t('显示最新文章'),
    //         'ShowRecentComments' => _t('显示最近回复'),
    //         'ShowCategory'       => _t('显示分类'),
    //         'ShowArchive'        => _t('显示归档'),
    //         'ShowOther'          => _t('显示其它杂项')
    //     ],
    //     ['ShowRecentPosts', 'ShowRecentComments', 'ShowCategory', 'ShowArchive', 'ShowOther'],
    //     _t('侧边栏显示')
    // );

    // $form->addInput($sidebarBlock->multiMode());
    $friendLink = new Typecho_Widget_Helper_Form_Element_Textarea('friendLink', NULL, NULL, _t('页脚友情链接'));
    $form->addInput($friendLink);
    
}

//获取文章缩略图，没有则随机
function get_ArticleThumbnail($widget){
  // 当文章无图片时的随机缩略图
  $rand = mt_rand(1, 5); // 随机 1-9 张缩略图
  // 缩略图加速
  $rand_url;
  if(!empty(Helper::options()->articleImgSpeed)){
    $rand_url = Helper::options()->articleImgSpeed;
  }else {
    $rand_url = $widget->widget('Widget_Options')->themeUrl . '/img/articles/';
  }
  $random =  $rand_url . $rand . '.avif'; // 随机缩略图路径

  $attach = $widget->attachments(1)->attachment;
  $pattern = '/\<img.*?src\=\"(.*?)\"[^>]*>/i';

  //如果有自定义缩略图
  if($widget->fields->thumb) {
    return $widget->fields->thumb;
  }else if (preg_match_all($pattern, $widget->content, $thumbUrl) && strlen($thumbUrl[1][0]) > 7) {
      return $thumbUrl[1][0];
  } else if ($attach->isImage) {
      return $attach->url;
  } else {
      return $random;
  }
};

//在线人数
function online_users() {
    $filename='online.txt'; //数据文件
    $cookiename='Nanlon_OnLineCount'; //Cookie名称
    $onlinetime=30; //在线有效时间
    $online=file($filename); 
    $nowtime=$_SERVER['REQUEST_TIME']; 
    $nowonline=array(); 
    foreach($online as $line){ 
        $row=explode('|',$line); 
        $sesstime=trim($row[1]); 
        if(($nowtime - $sesstime)<=$onlinetime){
            $nowonline[$row[0]]=$sesstime;
        } 
    } 
    if(isset($_COOKIE[$cookiename])){
        $uid=$_COOKIE[$cookiename]; 
    }else{
        $vid=0;
        do{
            $vid++; 
            $uid='U'.$vid; 
        }while(array_key_exists($uid,$nowonline)); 
        setcookie($cookiename,$uid); 
    } 
    $nowonline[$uid]=$nowtime;
    $total_online=count($nowonline); 
    if($fp=@fopen($filename,'w')){ 
        if(flock($fp,LOCK_EX)){ 
            rewind($fp); 
            foreach($nowonline as $fuid=>$ftime){ 
                $fline=$fuid.'|'.$ftime."\n"; 
                @fputs($fp,$fline); 
            } 
            flock($fp,LOCK_UN); 
            fclose($fp); 
        } 
    } 
    echo "$total_online"; 
} 

// 首页区块预留函数
function getNextNumber() {  
    static $counter = 00; // 声明一个静态变量 $counter，并在第一次调用时初始化为0  
    $counter++; // 每次调用时递增 $counter  
    return str_pad($counter, 2, '0', STR_PAD_LEFT); // 返回递增后的值  
} 
/* 根据评论agent获取设备类型 */
function _getAgentOS($agent)
{
	$os = "Linux";
	if (preg_match('/win/i', $agent)) {
		if (preg_match('/nt 6.0/i', $agent)) {
			$os = 'Windows Vista';
		} else if (preg_match('/nt 6.1/i', $agent)) {
			$os = 'Windows 7';
		} else if (preg_match('/nt 6.2/i', $agent)) {
			$os = 'Windows 8';
		} else if (preg_match('/nt 6.3/i', $agent)) {
			$os = 'Windows 8.1';
		} else if (preg_match('/nt 5.1/i', $agent)) {
			$os = 'Windows XP';
		} else if (preg_match('/nt 10.0/i', $agent)) {
			$os = 'Windows 10';
		} else {
			$os = 'Windows X64';
		}
	} else if (preg_match('/android/i', $agent)) {
		if (preg_match('/android 9/i', $agent)) {
			$os = 'Android Pie';
		} else if (preg_match('/android 8/i', $agent)) {
			$os = 'Android Oreo';
		} else {
			$os = 'Android';
		}
	} else if (preg_match('/ubuntu/i', $agent)) {
		$os = 'Ubuntu';
	} else if (preg_match('/linux/i', $agent)) {
		$os = 'Linux';
	} else if (preg_match('/iPhone/i', $agent)) {
		$os = 'iPhone';
	} else if (preg_match('/mac/i', $agent)) {
		$os = 'MacOS';
	} else if (preg_match('/fusion/i', $agent)) {
		$os = 'Android';
	} else {
		$os = 'Linux';
	}
	echo $os;
}
/* 根据评论agent获取浏览器类型 */
function _getAgentBrowser($agent)
{
	if (preg_match('/MSIE\s([^\s|;]+)/i', $agent, $regs)) {
		$outputer = 'Internet Explore';
	} else if (preg_match('/FireFox\/([^\s]+)/i', $agent, $regs)) {
		$outputer = 'FireFox';
	} else if (preg_match('/Maxthon([\d]*)\/([^\s]+)/i', $agent, $regs)) {
		$outputer = 'MicroSoft Edge';
	} else if (preg_match('#360([a-zA-Z0-9.]+)#i', $agent, $regs)) {
		$outputer = '360 Fast Browser';
	} else if (preg_match('/Edge([\d]*)\/([^\s]+)/i', $agent, $regs)) {
		$outputer = 'MicroSoft Edge';
	} else if (preg_match('/UC/i', $agent)) {
		$outputer = 'UC Browser';
	} else if (preg_match('/QQ/i', $agent, $regs) || preg_match('/QQ Browser\/([^\s]+)/i', $agent, $regs)) {
		$outputer = 'QQ Browser';
	} else if (preg_match('/UBrowser/i', $agent, $regs)) {
		$outputer = 'UC Browser';
	} else if (preg_match('/Opera[\s|\/]([^\s]+)/i', $agent, $regs)) {
		$outputer = 'Opera';
	} else if (preg_match('/Chrome([\d]*)\/([^\s]+)/i', $agent, $regs)) {
		$outputer = 'Google Chrome';
	} else if (preg_match('/safari\/([^\s]+)/i', $agent, $regs)) {
		$outputer = 'Safari';
	} else {
		$outputer = 'Google Chrome';
	}
	echo $outputer;
}
/* 获取父级评论 */
function _getParentReply($parent)
{
	if ($parent !== "0") {
		$db = Typecho_Db::get();
		$commentInfo = $db->fetchRow($db->select('author')->from('table.comments')->where('coid = ?', $parent));
		if (!empty($commentInfo['author'])) {
			echo '<div class="parent"><span style="vertical-align: 1px;">@</span> ' . $commentInfo['author'] . '</div>';
		}
	}
}
/* 过滤短代码 */
function _checkXSS($text)
{
    $isXss = false;
    $list = array(
        '/onabort/is',
        '/onblur/is',
        '/onchange/is',
        '/onclick/is',
        '/ondblclick/is',
        '/onerror/is',
        '/onfocus/is',
        '/onkeydown/is',
        '/onkeypress/is',
        '/onkeyup/is',
        '/onload/is',
        '/onmousedown/is',
        '/onmousemove/is',
        '/onmouseout/is',
        '/onmouseover/is',
        '/onmouseup/is',
        '/onreset/is',
        '/onresize/is',
        '/onselect/is',
        '/onsubmit/is',
        '/onunload/is',
        '/eval/is',
        '/ascript:/is',
        '/style=/is',
        '/width=/is',
        '/width:/is',
        '/height=/is',
        '/height:/is',
        '/src=/is',
    );
    if (strip_tags($text)) {
        for ($i = 0; $i < count($list); $i++) {
            if (preg_match($list[$i], $text) > 0) {
                $isXss = true;
                break;
            }
        }
    } else {
        $isXss = true;
    };
    return $isXss;
}
/* 过滤表情 */
function _parseReply($text)
{
    
    $text = preg_replace_callback(
        '/\:\#\(\s*(doge|亲亲|偷笑|再见|发怒|发财|可爱|吐血|呆|呕吐|困|坏笑|大佬|大哭|委屈|害羞|尴尬|微笑|思考|惊吓|打脸|抓狂|抠鼻子|斜眼笑|无奈|晕|流汗|流鼻血|点赞|生气|生病|疑问|白眼|睡着|笑哭|腼腆|色|调皮|鄙视|闭嘴|难过|馋|黑人问号|鼓掌)\s*\)/is',
        function ($match) {
            return '<img class="owo_image lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="' . Helper::options()->themeUrl . '/assets/owo/bilibili/' . str_replace('%', '', urlencode($match[1])) . '.gif" alt="小电视"/>';
        },
        $text
    );
    $text = preg_replace_callback(
        '/\:\$\(\s*(3d眼镜|EDG|LPL|beluga|不好意思|不服吗|亲亲|伞兵|倚墙笑|值得肯定|偷偷看|傻笑|再见|出家人|击剑|加班|勉强笑|危险|发红包|吃手|吃瓜|吐血|吵架|呦吼|呲牙笑|哈士奇|哈士奇失去意识|哈士奇失望|哭泣|唱歌|喜欢|嘿哈|大笑|失去信号|失望|头秃|奋斗|好奇|好的|害羞|小丑|小偷|尬笑|尴尬|应援|开心|引起不适|微笑|思考|恶心|恶魔|恶魔恐惧|惊吓|惊吓白眼|惊讶|惬意|感动|愤怒|我看好你|手机相机|打咩|打牌|托腮|扶额|抠鼻|抬眼镜|拜托|捂嘴笑|捂脸|擦汗|放鞭炮|敬礼|整理发型|斗鸡眼|智慧的眼神|月饼|有没有搞错|正确|没招|波吉|泪奔|流汗微笑|流鼻涕|深思|滑稽|滑稽吃瓜|滑稽喝水|滑稽奶茶|滑稽柠檬|滑稽狂汗|滑稽被子|烦恼|熊熊|熊猫|熊猫唱歌|熊猫喜欢|熊猫失望|熊猫意外|熬夜|爆炸|牛年进宝|狂热|狗头|狗头围脖|狗头失望|狗头意外|狗头胖次|狗头花|狗头草|猪头|猪头意外|生病|电话|疑问|疼痛|痛哭|看穿一切|眩晕|睡觉|禁言|笑哭|纠结|绿帽|缺牙笑|翻白眼|老虎意外|耍酷|胡子|菜狗|菜狗花|蒙面滑稽|虎年进宝|被打|裂开|警告|读书|财神红包|超爱|这是啥|送福|送花|错误|阴险|难以置信|面具|饥渴|鬼脸|黑线|鼓掌)\s*\)/is',
        function ($match) {
            return '<img class="owo_image lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="' . Helper::options()->themeUrl . '/assets/owo/Heo/' . str_replace('%', '', urlencode($match[1])) . '.gif" alt="Heo"/>';
        },
        $text
    );
    $text = preg_replace_callback(
        '/\:\:\(\s*(呵呵|哈哈|吐舌|太开心|笑眼|花心|小乖|乖|捂嘴笑|滑稽|你懂的|不高兴|怒|汗|黑线|泪|真棒|喷|惊哭|阴险|鄙视|酷|啊|狂汗|what|疑问|酸爽|呀咩爹|委屈|惊讶|睡觉|笑尿|挖鼻|吐|犀利|小红脸|懒得理|勉强|爱心|心碎|玫瑰|礼物|彩虹|太阳|星星月亮|钱币|茶杯|蛋糕|大拇指|胜利|haha|OK|沙发|手纸|香蕉|便便|药丸|红领巾|蜡烛|音乐|灯泡|开心|钱|咦|呼|冷|生气|弱|吐血|狗头)\s*\)/is',
        function ($match) {
            return '<img class="owo_image lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="' . Helper::options()->themeUrl . '/assets/owo/paopao/' . str_replace('%', '', urlencode($match[1])) . '_2x.png" alt="表情"/>';
        },
        $text
    );
    $text = preg_replace_callback(
        '/\:\@\(\s*(高兴|小怒|脸红|内伤|装大款|赞一个|害羞|汗|吐血倒地|深思|不高兴|无语|亲亲|口水|尴尬|中指|想一想|哭泣|便便|献花|皱眉|傻笑|狂汗|吐|喷水|看不见|鼓掌|阴暗|长草|献黄瓜|邪恶|期待|得意|吐舌|喷血|无所谓|观察|暗地观察|肿包|中枪|大囧|呲牙|抠鼻|不说话|咽气|欢呼|锁眉|蜡烛|坐等|击掌|惊喜|喜极而泣|抽烟|不出所料|愤怒|无奈|黑线|投降|看热闹|扇耳光|小眼睛|中刀)\s*\)/is',
        function ($match) {
            return '<img class="owo_image lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="' . Helper::options()->themeUrl . '/assets/owo/aru/' . str_replace('%', '', urlencode($match[1])) . '_2x.png" alt="表情"/>';
        },
        $text
    );
    return $text;
}
/* 过滤评论回复 */
function _parseCommentReply($text)
{
    if (_checkXSS($text)) {
        echo "该回复疑似异常，已被系统拦截！";
    } else {
        $text = _parseReply($text);
        echo preg_replace('/\{!\{([^\"]*)\}!\}/', '<img class="lazyload draw_image" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="$1" alt="画图"/>', $text);
    }
}

function yiyan() {
$url = 'https://api.oddfar.com/yl/q.php?c=2004&encode=text';  
$result = null;  
$response = file_get_contents($url);  
echo $response;
}

function threadedComments($comments, $options)
{ ?>

		<li class="comment comment-parent comment-odd comment-by-user">
			<div class="comment-body">

					<div class="comment-author">
						<span class="author">
				<img width="48" height="48" class="avatar lazyload" src="https://q.qlogo.cn/headimg_dl?dst_uin=<?php echo $comments->mail ?>@qq.com&spec=640&img_type=jpg" data-src="https://q.qlogo.cn/headimg_dl?dst_uin=<?php echo $comments->mail ?>@qq.com&spec=640&img_type=jpg" alt="头像" /><?php $comments->author(); ?></span>
						<?php if ($comments->authorId === $comments->ownerId) : ?>
							<i class="master">作者</i>
						<?php endif; ?>
						<?php if ($comments->status === "waiting") : ?>
							<em class="waiting">（评论审核中...）</em>
						<?php endif; ?>
					</div>
					<p>
					    <?php _getParentReply($comments->parent) ?>
						<?php echo _parseCommentReply($comments->content); ?>
					</p>
					<div class="handle">
						<time class="date" datetime="<?php $comments->date('Y-m-d'); ?>"><?php $comments->date('Y-m-d'); ?></time>
						<span class="comment-reply" data-no-instant><?php $comments->reply('回复'); ?></span>
					</div>

			</div>
		</li>
		<?php if ($comments->children) : ?>
			<div class="comment-list__item-children">
				<?php $comments->threadedComments($options); ?>
			</div>
		<?php endif; ?>

<?php } ?>
