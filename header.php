<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<!DOCTYPE html>
<html lang="zxx">

<head>
<title><?php $this->archiveTitle([
            'category' => _t('分类 %s 下的文章'),
            'search'   => _t('包含关键字 %s 的文章'),
            'tag'      => _t('标签 %s 下的文章'),
            'author'   => _t('%s 发布的文章')
        ], '', ' - '); ?><?php $this->options->title(); ?></title>
<meta name="description" content="<?php $this->options->description(); ?>">
<meta name="keywords" content="<?php $this->options->keywords(); ?>">
<link rel="icon" type="image/x-icon" href="<?php $this->options->favicon(); ?>">
<meta charset="<?php $this->options->charset(); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<!-- bootstrap css -->
<link rel="stylesheet" href="<?php $this->options->themeUrl('/css/plugins/bootstrap.min.css'); ?>">
<!-- locomotive scroll css -->
<link rel="stylesheet" href="<?php $this->options->themeUrl('/css/plugins/locomotive-scroll.css'); ?>">
<!-- swiper css -->
<link rel="stylesheet" href="<?php $this->options->themeUrl('/css/plugins/swiper.min.css'); ?>">
<!-- fancybox css -->
<link rel="stylesheet" href="<?php $this->options->themeUrl('/css/plugins/fancybox.min.css'); ?>">
<!-- font awesome css -->
<link rel="stylesheet" href="<?php $this->options->themeUrl('/css/plugins/font-awesome.min.css'); ?>">
<!-- Semporia dark mode css <link rel="stylesheet" href="css/style-dark.css">-->
<!-- Semporia light mode css -->
<link rel="stylesheet" href="<?php $this->options->themeUrl('/css/style-light.css'); ?>" id="trm-switch-style">
<link rel="stylesheet" href="<?php $this->options->themeUrl('/style.css'); ?>" id="style">
<style>
:root{
    --light-color:<?php $this->options->light() ?>;
    --dark-color:<?php $this->options->dark() ?>;
}
path {
    fill: #fff;
}
.markdown-body h1, .markdown-body h2, .markdown-body h3, .markdown-body h4, .markdown-body h5, .markdown-body h6 {
margin-top: 24px;
margin-bottom: 16px;
font-weight: 600;
line-height: 1.25;
}
.markdown-body p, .markdown-body blockquote, .markdown-body ul, .markdown-body ol, .markdown-body dl, .markdown-body table, .markdown-body pre, .markdown-body details {
margin-top: 0;
margin-bottom: 16px;
}
.task-list-item {
list-style-type: none;
}
.task-list-item-checkbox {
margin: 0 0.2em 0.25em -1.6em;
vertical-align: middle;
}
.markdown-body pre {
padding: 16px;
overflow: auto;
font-size: 85%;
font-weight: bold;
line-height: 1.45;
color: #FFFFFF;
background-color: rgba(0, 0, 0, 0.6);
border-radius: 6px;
}
.markdown-body pre code, .markdown-body pre tt {
display: inline;
max-width: auto;
padding: 0;
margin: 0;
overflow: visible;
line-height: inherit;
word-wrap: normal;
background-color: transparent;
border: 0;
}
</style>
    <?php $this->header(); ?>
</head>

<body>
<!-- app wrapper -->
<div class="trm-app-frame">
<!-- page preloader -->
<div class="trm-preloader">
<div class="trm-holder">
<div class="preloader">
<div></div>
<div></div>
<div></div>
<div></div>
<div></div>
<div></div>
<div></div>
<div></div>
<div></div>
<div></div>
</div>
</div>
</div>
<!-- page preloader end -->

<div class="trm-hidden-switcher">
<div class="trm-mode-switcher"><i class="far fa-sun"></i>
<input class="tgl tgl-light" id="trm-swich" type="checkbox">
<label class="trm-swich" for="trm-swich"></label>
<i class="far fa-moon"></i>
</div>
</div>
<div class="trm-mode-swich-animation-frame">
<div class="trm-mode-swich-animation"><i class="far fa-sun"></i>
<div class="trm-horizon"></div>
<i class="far fa-moon"></i>
</div>
</div>
<!-- scroll container -->
<div id="trm-dynamic-content" class="trm-swup-animation">
<div id="trm-scroll-container" class="trm-scroll-container" data-scroll-container style="opacity: 0">
<div data-scroll-section id="content" class="trm-scroll-section">
<!-- top bar -->
<div class="trm-top-bar" data-scroll data-scroll-sticky data-scroll-target="#content" data-scroll-offset="-10">
<div class="container">
<div class="trm-left-side">
<!-- logo -->
<a href="<?php $this->options->siteUrl(); ?>" class="trm-logo-frame trm-anima-link">
    <div class="trm-logo-text">
        <span>
            <img src="<?php $this->options->logoUrl() ?>" alt="<?php $this->options->title() ?>"/>
        </span>
    </div>
</a>
<!-- logo end -->
</div>
<div class="trm-right-side">
<!-- menu -->
<div class="trm-menu">
<nav>
<ul>
<li<?php if ($this->is('index')): ?> class="current-menu-item"<?php endif; ?>>
    <a<?php if ($this->is('index')): ?><?php endif; ?> href="<?php $this->options->siteUrl(); ?>"><?php _e('首页'); ?></a>
</li>
<?php \Widget\Contents\Page\Rows::alloc()->to($pages); ?>
<?php while ($pages->next()): ?>
<li<?php if ($this->is('page', $pages->slug)): ?> class="current-menu-item"<?php endif; ?>><a<?php if ($this->is('page', $pages->slug)): ?><?php endif; ?> href="<?php $pages->permalink(); ?>" title="<?php $pages->title(); ?>"><?php $pages->title(); ?></a></li>
<?php endwhile; ?>
</ul>
</nav>
</div>
<!-- menu end -->
<!-- mode switcher place -->
<div class="trm-mode-switcher-place"></div>
<!-- mode switcher place end -->
<!-- action button -->
<!-- <a href="files/cv.txt" download class="trm-btn trm-btn-sm">Download cv <i class="fas fa-arrow-down"></i></a> -->
<!-- action button end -->
</div>
<div class="trm-menu-btn"><span></span></div>
</div>
</div>
<!-- top bar end -->
<div class="trm-content-start">
<!-- banner -->
<div class="trm-banner" data-scroll data-scroll-direction="vertical" data-scroll-speed="-1">
<!-- 背景 -->
<?php if ($this->is('index')): ?>
<img src="<?php $this->options->bg1() ?>" alt="banner" class="trm-banner-cover" data-scroll data-scroll-direction="vertical" data-scroll-speed="-4">
<?php else: ?>
<img src="<?php $this->options->bg2() ?>" alt="banner" class="trm-banner-cover" data-scroll data-scroll-direction="vertical" data-scroll-speed="-4">
<?php endif; ?>

<div class="trm-banner-content trm-overlay">
<div class="container" data-scroll data-scroll-direction="vertical" data-scroll-speed="1">
<div class="row">
<div class="col-lg-4"></div>
<div class="col-lg-8">
<!-- 首页简介 -->
<div class="trm-banner-text">
<div class="trm-label trm-mb-20"><?php $this->options->description() ?></div>
<h1 class="trm-mb-30">  
    <?php  
    if ($this->is('index')) {  
        // 在首页上显示站点的标题  
        echo $this->options->title();  
    } else {  
        echo $this->title();  
    }  
    ?>  
</h1>

<ul class="trm-breadcrumbs trm-label">
	<?php if ($this->is('index')): ?><!-- 页面为首页时 -->
		Latest Post
	<?php elseif ($this->is('post')): ?><!-- 页面为文章单页时 -->
		<li><a href="<?php $this->options->siteUrl(); ?>">HOME</a></li><li><?php $this->category(); ?></li><li><span><?php $this->title() ?></span></li>
	<?php else: ?><!-- 页面为其他页时 -->
		<li><span><?php $this->archiveTitle('','',''); ?></span></li>
	<?php endif; ?>
</ul>

</div>
<!-- banner title end -->
<!-- scroll hint -->
<a href="#about-triger" data-scroll-to="#about-triger" data-scroll-offset="-130"
class="trm-scroll-hint-frame">
<div class="trm-scroll-hint"></div>
<span class="trm-label">向下滚动</span>
</a><!-- scroll hint end -->
</div>
</div>
</div>
</div>
<!-- banner content end -->
</div>
<!-- banner end -->
<div class="container">
<div class="row">
<div class="col-lg-4">
<!-- main card -->
<div class="trm-main-card-frame trm-sidebar">
<div class="trm-main-card" data-scroll data-scroll-repeat data-scroll-sticky
data-scroll-target="#content" data-scroll-offset="60">
<!-- 头像区块 已替换成QQ头像 -->
<div class="trm-mc-header">
<div class="trm-avatar-frame trm-mb-20"><img class="trm-avatar" src="https://q.qlogo.cn/headimg_dl?dst_uin=<?php $this->options->QQ() ?>&spec=640&img_type=jpg"
alt="Avatar">
<div class="trm-dot"></div>
</div>
<h5 class="trm-name trm-mb-15"><?php $this->author->screenName(); ?></h5>
<div class="trm-label">我是一名 <span class="trm-typed-text">
<!-- Words for rotation:js/main.js line 34 -->
</span></div>
</div>
<!-- 头像区块结束 -->
<div class="trm-divider trm-mb-40 trm-mt-40"></div>
<!-- 头像下方图标区块 -->
<div class="trm-social">
<a href="https://wpa.qq.com/wpa_jump_page?v=3&uin=<?php $this->options->QQ() ?>&site=qq&menu=yes" target="_blank"><i class="fab fa-qq"></i></a>
<a href="<?php $this->options->github() ?>" target="_blank"><i class="fab fa-github"></i></a>
<a href="<?php $this->options->telegraph() ?>" target="_blank"><i class="fab fa-telegram"></i></a>
<a href="<?php $this->options->twitter() ?>" target="_blank"><i class="fab fa-twitter"></i></a>
</div>
<!-- 头像图标区块结束 -->
<!-- <div class="trm-divider trm-mb-40 trm-mt-40"></div> -->
<!-- info -->
<!-- <ul class="trm-table trm-mb-20">
<li>
<div class="trm-label">Residence:</div>
<div class="trm-label trm-label-light">Home</div>
</li>
<li>
<div class="trm-label">City:</div>
<div class="trm-label trm-label-light">Cheng Du</div>
</li>
<li>
<div class="trm-label">Age:</div>
<div class="trm-label trm-label-light">29</div>
</li>
</ul> -->
<!-- info end -->
<div class="trm-divider trm-mb-40 trm-mt-40"></div>
<!-- 头像下方按钮 -->
<div class="text-center">
<a href="mailto:<?php $this->options->email() ?>" class="trm-btn">联系我 <i class="far fa-envelope"></i></a>
<!-- <a data-fancybox href="#trm-order" class="trm-btn">Contact me <i class="far fa-envelope"></i></a> -->
</div>
<!-- 头像下方按钮结束 -->
</div>
</div>
<!-- main card end -->
</div>